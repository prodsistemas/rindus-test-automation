Feature: Shopping Cart Calculation

  In order to verify the product price is correctly calculated
  as an amazon consumer
  I want to modify the quantity of the selected product

  Scenario: Verify that the product price is correctly calculated when quantity is modified
    Given "Pedro" is searching for "hats for men"
    When he adds 2 pieces of the first hat that appears
    Then the total price and quantity displayed in the cart should be correct
    And the total price and quantity should be correctly changed once the quantity of the selected product is reduced to 1