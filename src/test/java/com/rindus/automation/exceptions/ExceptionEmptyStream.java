package com.rindus.automation.exceptions;

public class ExceptionEmptyStream extends Exception {
    public ExceptionEmptyStream(String message) {
        super(message);
    }
}
