package com.rindus.automation.exceptions;

public class ExceptionNotSuchElement extends Exception{
    public ExceptionNotSuchElement(String message) {
        super(message);
    }
}
