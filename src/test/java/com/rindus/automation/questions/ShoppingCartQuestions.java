package com.rindus.automation.questions;

import com.rindus.automation.tasks.shoppingcart.ShoppingCartTasks;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

import static org.assertj.core.api.Assertions.assertThat;

public class ShoppingCartQuestions {

    @Steps
    ShoppingCartTasks shoppingCart;

    @Step
    public void isTotalPriceAndQuantityCorrect() {
        int actualTotalItems = shoppingCart.getTotalItems();
        double actualTotalPrice = shoppingCart.getTotalPrice();
        int expectedTotalItems = Serenity.sessionVariableCalled("expectedTotalItems");
        double expectedTotalPrice = Serenity.sessionVariableCalled("productPrice");
        expectedTotalPrice *= expectedTotalItems;
        assertThat(actualTotalItems)
                .as("TOTAL ITEMS COMPARISON")
                .withFailMessage("Total items does not match : Expected < %d > but was < %d >",expectedTotalItems, actualTotalItems)
                .isEqualTo(expectedTotalItems);
        assertThat(actualTotalPrice)
                .as("TOTAL PRICE COMPARISON")
                .withFailMessage("Total price does not match : Expected < %f > but was < %f >",expectedTotalPrice, actualTotalPrice)
                .isEqualTo(expectedTotalPrice);
    }
}
