package com.rindus.automation.utils;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class StringManagement {
    public static String extractNumbersFrom(String extractInString) {
        String regEx="[^0-9]+";
        Pattern pattern = Pattern.compile(regEx);
        List<String> cs = Arrays.asList(pattern.split(extractInString));
        return String.join("", cs);
    }
}
