package com.rindus.automation.tasks.navigation;

import com.rindus.automation.exceptions.ExceptionEmptyStream;
import com.rindus.automation.tasks.navigation.locators.HeaderMenuLocators;
import lombok.SneakyThrows;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class HeaderMenuTasks extends UIInteractionSteps {

    @Step
    public void shoppingCartDetails() {
        selectHeaderMenuItem("Cart");
    }

    @SneakyThrows
    private void selectHeaderMenuItem(String menuItem) {
        waitFor(ExpectedConditions.elementToBeClickable(HeaderMenuLocators.LST_NAV_RIGHT));
        $$(HeaderMenuLocators.LST_NAV_RIGHT).stream()
                .filter(item -> item.getText().contains("Cart"))
                .findFirst()
                .orElseThrow(() -> new ExceptionEmptyStream(String.format("None menu item contains the word %s", menuItem)))
                .click();
    }
}
