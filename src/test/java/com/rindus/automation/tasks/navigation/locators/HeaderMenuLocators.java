package com.rindus.automation.tasks.navigation.locators;

import org.openqa.selenium.By;

public class HeaderMenuLocators {

    private HeaderMenuLocators() {
        throw new IllegalStateException("Header Menu Locators class");
    }

    public static final By LST_NAV_RIGHT = By.cssSelector(".nav-right a");
}
