package com.rindus.automation.tasks.navigation;

import net.thucydides.core.annotations.Step;

public class NavigationTasks {

    AmazonHomePage amazonHomePage;

    @Step
    public void theHomePage() {
        amazonHomePage.open();
    }
}
