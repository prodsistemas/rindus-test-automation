package com.rindus.automation.tasks.search;

import com.rindus.automation.tasks.search.locators.SearchLocators;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class SearchTasks extends UIInteractionSteps {

    @Step
    public void searchingFor(String product) {
        waitFor(ExpectedConditions.elementToBeClickable(SearchLocators.TXT_SEARCH));
        $(SearchLocators.TXT_SEARCH).sendKeys(product, Keys.ENTER);
    }
}
