package com.rindus.automation.tasks.search.locators;

import org.openqa.selenium.By;

public class SearchLocators {

    private SearchLocators() {
        throw new IllegalStateException("Search Locators class");
    }

    public static final By TXT_SEARCH = By.id("twotabsearchtextbox");
}
