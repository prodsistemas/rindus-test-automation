package com.rindus.automation.tasks.shoppingcart;

import com.rindus.automation.exceptions.ExceptionEmptyStream;
import com.rindus.automation.tasks.shoppingcart.locators.ShoppingCartLocators;
import com.rindus.automation.utils.StringManagement;
import lombok.SneakyThrows;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ShoppingCartTasks extends UIInteractionSteps {

    public double getTotalPrice() {
        String totalPrice = $(ShoppingCartLocators.LBL_TOTAL_PRICE).getText();
        return Double.valueOf(totalPrice.replace("$", ""));
    }

    public int getTotalItems() {
        String totalItems = $(ShoppingCartLocators.LBL_TOTAL_ITEMS).getText();
        totalItems = StringManagement.extractNumbersFrom(totalItems);
        return Integer.valueOf(totalItems);
    }

    @Step
    public void modifyQuantityTo(int quantity) {
        $(ShoppingCartLocators.BTN_SELECT_QUANTITY).click();
        selectNewQuantity(quantity);
        waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("body .sc-list-item-overwrap")));
    }

    @SneakyThrows
    private void selectNewQuantity(int quantity) {
        waitFor(ExpectedConditions.visibilityOf($(ShoppingCartLocators.DRD_QUANTITY)));
        $$(ShoppingCartLocators.DRD_QUANTITY).stream()
                .filter(q -> Integer.valueOf(StringManagement.extractNumbersFrom(q.getText())).equals(quantity))
                .findFirst()
                .orElseThrow(() -> new ExceptionEmptyStream(String.format("Quantity %d is not in the list", quantity)))
                .click();
    }
}
