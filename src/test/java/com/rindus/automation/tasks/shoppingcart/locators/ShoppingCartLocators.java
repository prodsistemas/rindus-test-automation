package com.rindus.automation.tasks.shoppingcart.locators;

import org.openqa.selenium.By;

public class ShoppingCartLocators {

    private ShoppingCartLocators() {
        throw new IllegalStateException("Shopping Cart Locators class");
    }

    public static final By LBL_TOTAL_PRICE = By.cssSelector("#sc-active-cart .sc-subtotal .sc-price");
    public static final By LBL_TOTAL_ITEMS = By.cssSelector("#sc-active-cart .sc-subtotal .sc-number-of-items");
    public static final By BTN_SELECT_QUANTITY = By.cssSelector("select#quantity + span");
    public static final By DRD_QUANTITY = By.cssSelector("body .a-popover li");
}
