package com.rindus.automation.tasks.products;

import com.rindus.automation.exceptions.ExceptionEmptyStream;
import com.rindus.automation.tasks.products.locators.ProductDetailsLocators;
import lombok.SneakyThrows;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ProductDetailsTasks extends UIInteractionSteps {

    @SneakyThrows
    @Step
    public void addToCartWithQuantity(int quantity) {
        double productPrice = Double.valueOf(getPrice());
        Serenity.setSessionVariable("productPrice").to(productPrice);
        $(ProductDetailsLocators.BTN_SELECT_QUANTITY).click();
        selectQuantity(quantity);
        $(ProductDetailsLocators.BTN_ADD_TO_CART).click();
    }

    @NotNull
    private String getPrice() {
        return $(ProductDetailsLocators.LBL_PRICE).getText().replace("$", "");
    }

    @SneakyThrows
    private void selectQuantity(int quantity) {
        waitFor(ExpectedConditions.visibilityOf($(ProductDetailsLocators.DRD_QUANTITY)));
        $$(ProductDetailsLocators.DRD_QUANTITY).stream()
                .filter(q -> Integer.valueOf(q.getText()).equals(quantity))
                .findFirst()
                .orElseThrow(() -> new ExceptionEmptyStream(String.format("Quantity %d is not in the list", quantity)))
                .click();
    }
}
