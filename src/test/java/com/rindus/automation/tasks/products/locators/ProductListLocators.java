package com.rindus.automation.tasks.products.locators;

import org.openqa.selenium.By;

public class ProductListLocators {

    private ProductListLocators() {
        throw new IllegalStateException("Product List Locators class");
    }

    public static final By LST_PRODUCTS = By.cssSelector("body .s-result-list > div.s-result-item:not(:first-child):not(div[data-component-type='']) .a-price");
}
