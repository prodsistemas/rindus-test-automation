package com.rindus.automation.tasks.products.locators;

import org.openqa.selenium.By;

public class ProductDetailsLocators {

    private ProductDetailsLocators() {
        throw new IllegalStateException("Product Details Locators class");
    }

    public static final By BTN_SELECT_QUANTITY = By.cssSelector("#selectQuantity select + span");
    public static final By DRD_QUANTITY = By.cssSelector("body .a-popover li");
    public static final By BTN_ADD_TO_CART = By.id("add-to-cart-button");
    public static final By LBL_PRICE = By.id("price_inside_buybox");
}
