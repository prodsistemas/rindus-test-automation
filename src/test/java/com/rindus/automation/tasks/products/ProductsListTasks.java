package com.rindus.automation.tasks.products;

import com.rindus.automation.exceptions.ExceptionEmptyStream;
import com.rindus.automation.tasks.products.locators.ProductListLocators;
import lombok.SneakyThrows;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ProductsListTasks extends UIInteractionSteps {

    @SneakyThrows
    @Step
    public void selectTheFirstProduct() {
        waitFor(ExpectedConditions.elementToBeClickable(ProductListLocators.LST_PRODUCTS));
        $$(ProductListLocators.LST_PRODUCTS).stream()
                .findFirst()
                .orElseThrow(() -> new ExceptionEmptyStream("None product in the list"))
                .click();
    }
}
