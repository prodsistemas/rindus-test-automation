package com.rindus.automation.stepdefinitions;

import com.rindus.automation.questions.ShoppingCartQuestions;
import com.rindus.automation.tasks.navigation.HeaderMenuTasks;
import com.rindus.automation.tasks.navigation.NavigationTasks;
import com.rindus.automation.tasks.products.ProductDetailsTasks;
import com.rindus.automation.tasks.products.ProductsListTasks;
import com.rindus.automation.tasks.search.SearchTasks;
import com.rindus.automation.tasks.shoppingcart.ShoppingCartTasks;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

public class CartCalculationStepDefinitions {

    @Steps
    NavigationTasks navigateTo;
    @Steps
    HeaderMenuTasks headerMenu;
    @Steps
    SearchTasks search;
    @Steps
    ProductsListTasks productsList;
    @Steps
    ProductDetailsTasks productDetails;
    @Steps
    ShoppingCartTasks shoppingCart;
    @Steps
    ShoppingCartQuestions shoppingCartQuestions;

    @Given("{string} is searching for {string}")
    public void searchingNewProduct(String actor, String product) {
        navigateTo.theHomePage();
        search.searchingFor(product);
    }

    @When("he adds {int} pieces of the first hat that appears")
    public void addNewProductToTheCart(int quantity) {
        Serenity.setSessionVariable("expectedTotalItems").to(quantity);
        productsList.selectTheFirstProduct();
        productDetails.addToCartWithQuantity(quantity);
    }

    @Then("the total price and quantity displayed in the cart should be correct")
    public void verifyTheTotalPriceAndQuantityAreCorrect() {
        headerMenu.shoppingCartDetails();
        shoppingCartQuestions.isTotalPriceAndQuantityCorrect();
    }

    @Then("the total price and quantity should be correctly changed once the quantity of the selected product is reduced to {int}")
    public void verifyTheTotalPriceAndQuantityAreCorrectlyModified(int quantity) {
        Serenity.setSessionVariable("expectedTotalItems").to(quantity);
        shoppingCart.modifyQuantityTo(quantity);
        shoppingCartQuestions.isTotalPriceAndQuantityCorrect();
    }
}
