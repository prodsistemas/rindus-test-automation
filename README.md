### Project Repository - Downloading the Framework

rindus-test-automation framework source code can be downloaded from:

https://bitbucket.org/prodsistemas/rindus-test-automation/src/master/

## Framework configuration files
- serenity.conf         (WebdriverManager settings to download webdriver binaries)
- serenity.properties   (Global execution settings: Implicit waits, explicit waits)

### Project directory structure
The project has a build script for Maven, and follows a customized directory structure:
```Gherkin
src
  + test
    + java                          Test runners and supporting code
      + com.rindus.automation
        + exceptions                Complementary classes for exceptions customization
        + questions                 Assertion classes for result comparison            
        + stepdefinitions           Step definitions for step sequence mapping and execution
        + tasks                     Tasks classes and Locators classes sorted by page component
        + utils                     Additional classes for common 
        CucumberTestSuite.java
    + resources
      + features                    Feature files sorted by epic
      serenity.conf                 Serenity configuration file
```

### Sample scenario 

```Gherkin
Feature: Shopping Cart Calculation

  In order to verify the product price is correctly calculated
  as an amazon consumer
  I want to modify the quantity of the selected product

  Scenario: Verify that the product price is correctly calculated when quantity is modified
    Given "Pedro" is searching for "hats for men"
    When he adds 2 pieces of the first hat that appears
    Then the total price and quantity displayed in the cart should be correct
    And the total price and quantity should be correctly changed once the quantity of the selected product is reduced to 1
```

### The Action Classes implementation.

The glue code for each step:

```java
@Given("{string} is searching for {string}")
public void searchingNewProduct(String actor, String product) {
    navigateTo.theHomePage();
    search.searchingFor(product);
}

@When("he adds {int} pieces of the first hat that appears")
public void addNewProductToTheCart(int quantity) {
    Serenity.setSessionVariable("expectedTotalItems").to(quantity);
    productsList.selectTheFirstProduct();
    productDetails.addToCartWithQuantity(quantity);
}

@Then("the total price and quantity displayed in the cart should be correct")
public void verifyTheTotalPriceAndQuantityAreCorrect() {
    headerMenu.shoppingCartDetails();
    shoppingCartQuestions.isTotalPriceAndQuantityCorrect();
}

@Then("the total price and quantity should be correctly changed once the quantity of the selected product is reduced to {int}")
public void verifyTheTotalPriceAndQuantityAreCorrectlyModified(int quantity) {
    Serenity.setSessionVariable("expectedTotalItems").to(quantity);
    shoppingCart.modifyQuantityTo(quantity);
    shoppingCartQuestions.isTotalPriceAndQuantityCorrect();
}
```

These classes are declared using the Serenity `@Steps` annotation, shown below:
```java
@Steps
NavigationTasks navigateTo;
@Steps
HeaderMenuTasks headerMenu;
@Steps
SearchTasks search;
@Steps
ProductsListTasks productsList;
@Steps
ProductDetailsTasks productDetails;
@Steps
ShoppingCartTasks shoppingCart;
@Steps
ShoppingCartQuestions shoppingCartQuestions;
```

The `@Steps`annotation tells Serenity to create a new instance of the class, and inject any other steps or page objects that this instance might need.

Each action class models a particular facet of user behaviour: navigating to a particular page, performing a search, or retrieving the results of a search. These classes are designed to be small and self-contained, which makes them more stable and easier to maintain.

- The `NavigationTasks` class is an example of a very simple action class. It just opens the Amazon home page:
```java
public class NavigationTasks {

    AmazonHomePage amazonHomePage;

    @Step
    public void theHomePage() {
        amazonHomePage.open();
    }
}
```

It does this using a standard Serenity Page Object. Page Objects are often very minimal, storing just the URL of the page itself:
```java
@DefaultUrl("https://www.amazon.com")
public class AmazonHomePage extends PageObject {}
```

- The `ProductDetailsTasks` class, is an interaction class. We make the class extend the Serenity `UIInteractionSteps`. This gives the class full access to the powerful Serenity WebDriver API, including the `$()` method used below, which locates a web element using a `By` locator or an XPath or CSS expression:
```java
public class ProductDetailsTasks extends UIInteractionSteps {

    @SneakyThrows
    @Step
    public void addToCartWithQuantity(int quantity) {
        double productPrice = Double.valueOf(getPrice());
        Serenity.setSessionVariable("productPrice").to(productPrice);
        $(ProductDetailsLocators.BTN_SELECT_QUANTITY).click();
        selectQuantity(quantity);
        $(ProductDetailsLocators.BTN_ADD_TO_CART).click();
    }

    @NotNull
    private String getPrice() {
        return $(ProductDetailsLocators.LBL_PRICE).getText().replace("$", "");
    }

    @SneakyThrows
    private void selectQuantity(int quantity) {
        waitFor(ExpectedConditions.visibilityOf($(ProductDetailsLocators.DRD_QUANTITY)));
        $$(ProductDetailsLocators.DRD_QUANTITY).stream()
                .filter(q -> Integer.valueOf(q.getText()).equals(quantity))
                .findFirst()
                .orElseThrow(() -> new ExceptionEmptyStream(String.format("Quantity %d is not in the list", quantity)))
                .click();
    }
}
```

- Next class `ProductDetailsLocators` declares the elements located in a specific component. In this case those elements belongs to Product Details component:
```java
public class ProductDetailsLocators {

    private ProductDetailsLocators() {
        throw new IllegalStateException("Product Details Locators class");
    }

    public static final By BTN_SELECT_QUANTITY = By.cssSelector("#selectQuantity select + span");
    public static final By DRD_QUANTITY = By.cssSelector("body .a-popover li");
    public static final By BTN_ADD_TO_CART = By.id("add-to-cart-button");
    public static final By LBL_PRICE = By.id("price_inside_buybox");
}
```

- The last class `ShoppingCartQuestions` decouples assertions methods in order to compare expected results with actual results:
```java
public class ShoppingCartQuestions {

    @Steps
    ShoppingCartTasks shoppingCart;

    @Step
    public void isTotalPriceAndQuantityCorrect() {
        int actualTotalItems = shoppingCart.getTotalItems();
        double actualTotalPrice = shoppingCart.getTotalPrice();
        int expectedTotalItems = Serenity.sessionVariableCalled("expectedTotalItems");
        double expectedTotalPrice = Serenity.sessionVariableCalled("productPrice");
        expectedTotalPrice *= expectedTotalItems;
        assertThat(actualTotalItems)
                .as("TOTAL ITEMS COMPARISON")
                .withFailMessage("Total items does not match : Expected < %d > but was < %d >",expectedTotalItems, actualTotalItems)
                .isEqualTo(expectedTotalItems);
        assertThat(actualTotalPrice)
                .as("TOTAL PRICE COMPARISON")
                .withFailMessage("Total price does not match : Expected < %f > but was < %f >",expectedTotalPrice, actualTotalPrice)
                .isEqualTo(expectedTotalPrice);
    }
}
```


### Executing the tests
To run the rindus-test-automation project, you can either just run the `CucumberTestSuite` test runner class, or run either `mvn clean verify` from the command line.

```json
$ mvn clean verify
```

The test results will be recorded in `target/site/serenity/index.html` path.
 
### Local execution - Webdriver configuration
The WebDriver configuration is managed entirely from 'serenity.conf' file:
```java
webdriver {
    driver = chrome
    base.url = "https://www.amazon.com"
    timeouts.implicitlywait = 20000
    timeouts.fluentwait = 10000
    autodownload = true
}

serenity.take.screenshots = FOR_FAILURES

headless.mode = false

serenity {
    project.name = "Rindus Test Automation"
    test.root = "src.test.resources.features"
    tag.failures = "true"
    linked.tags = "issue"
}

home.page = "https://www.amazon.com"
restart.browser.each.scenario = true

chrome {
    switches = "--start-maximized;--enable-automation;--no-sandbox;--disable-popup-blocking;--disable-default-apps;--disable-infobars;--disable-gpu;--disable-extensions;"
}

```